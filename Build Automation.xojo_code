#tag BuildAutomation
			Begin BuildStepList Linux
				Begin BuildProjectStep Build
				End
			End
			Begin BuildStepList Mac OS X
				Begin IDEScriptBuildStep setShortVersionMAC , AppliesTo = 0
					
					// Set the ShortVersion to match the
					// version numbers
					Dim version As String
					version = PropertyValue("App.MajorVersion") + "." + _
					PropertyValue("App.MinorVersion") + "." + _
					PropertyValue("App.BugVersion") + " (" + _
					PropertyValue("App.NonReleaseVersion") + ")"
					
					PropertyValue("App.ShortVersion") = version
					
					'' BROKEN
					Dim lversion as string
					lversion = "XoDrill" + _
					"   v" + version + _
					",  " +  chr( 169 ) + "2015"
					
					PropertyValue("App.LongVersion") = lversion
				End
				Begin IDEScriptBuildStep SaveScriptOSX , AppliesTo = 0
					DoCommand "SaveFile"
				End
				Begin BuildProjectStep Build
				End
			End
			Begin BuildStepList Windows
				Begin BuildProjectStep Build
				End
			End
#tag EndBuildAutomation
