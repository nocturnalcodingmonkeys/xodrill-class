# [MOTHBALLED! we are no longer developing these classes] #

# README #

This a a Xojo class to use the Mandrill APIs from MailChimp.  You must open an account with Mandrill (https://mandrillapp.com) and get an API key.

These class(es) do not use any plugins or 3rd party modules.  The only module they use are the Xojo Unit Testing (XUT) module that comes with Xojo.  If you do not want to use XUT, just remove the XUT tests.

### How do I get set up? ###

* copy the XoDrill folder from this application code to your application code within Xojo IDE.
* get an API Key from Mandrill
* have fun.

### documentation ###

* [XoDrillIps](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillIps.md)
* [XoDrillMessage](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillMessage.md)
* [XoDrillMetadata](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillMetadata.md)
* [XoDrillSenders](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillSenders.md)
* [XoDrillTags](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillTags.md)
* [XoDrillTemplates](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillTemplates.md)
* [XoDrillUser](/nocturnalcodingmonkeys/xodrill-class/src/master/XoDrill/XoDrillUser.md)

the API calls that are not implemented are noted in the docs under **TBD methods**.

note: all methods return the same data type as the mandrill API (JSONItem aka JSON) unless the name has "As<Datatype>" in it.

### Who do I talk to? ###

* contact support at nocturnalcodingmonkeys.com if you have any issues.

### releases ###

* 2015-03-09 - v0.5.0 (37)
* 2015-02-23 - v0.4.0 (34)
* 2015-02-23 - v0.3.0 (30)
* 2015-02-22 - v0.2.1 (27) - BUG FIX RELEASE
* 2015-01-15 - v0.2.0 (25)
