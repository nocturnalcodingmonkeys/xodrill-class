# XoDrillMetadata class

base URL for the class: https://mandrillapp.com/api/1.0/metadata/*

## completed methods

* add() as JSONItem
* delete() as JSONItem
* list() as JSONItem
* update() as JSONItem


\* methods are methods that return a non-standard for Mandril API return type.



## valid as of

valid as of GIT: 0.5.0 (37)