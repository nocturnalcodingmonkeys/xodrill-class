# XoDrillMessage class

base URL for the class: https://mandrillapp.com/api/1.0/message/*

## completed methods

* cancelScheduled() as JSONItem
* content() as JSONItem
* info() as JSONItem
* listScheduled() as JSONItem
* send() as JSONItem
* buildMessage()**


\* methods are methods that return a non-standard for Mandril API return type.

\** method to build a message from the properties of the class so it can be sent with send().

## TBD methods

* send-template()
* search()
* search-time-series()
* parse()
* send-raw()
* reschedule()

## valid as of

valid as of GIT: 0.5.0 (37)