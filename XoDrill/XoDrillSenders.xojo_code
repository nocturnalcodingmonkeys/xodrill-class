#tag Class
Protected Class XoDrillSenders
	#tag Method, Flags = &h0
		Function addDomain() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if domain.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "domain"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/add-domain.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "domain" ) = domain
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.addDomain() " + stringReturn
		    System.DebugLog "XoDrillSenders.addDomain() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonInfo as new JSONItem( stringReturn )
		  
		  return jsonInfo
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function checkDomain() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if domain.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "domain"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/check-domain.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "domain" ) = domain
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.checkDomain() " + stringReturn
		    System.DebugLog "XoDrillSenders.checkDomain() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonInfo as new JSONItem( stringReturn )
		  
		  return jsonInfo
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function domains() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/domains.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.domains() " + stringReturn
		    System.DebugLog "XoDrillSenders.domains() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonDomains as new JSONItem( stringReturn )
		  
		  return jsonDomains
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function info() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if address.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "address"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/info.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "address" ) = address
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.info() " + stringReturn
		    System.DebugLog "XoDrillSenders.info() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonInfo as new JSONItem( stringReturn )
		  
		  return jsonInfo
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function list() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/list.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.list() " + stringReturn
		    System.DebugLog "XoDrillSenders.list() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonSenders as new JSONItem( stringReturn )
		  
		  return jsonSenders
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function timeSeries() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if address.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "address"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/time-series.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "address" ) = address
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.timeSeries() " + stringReturn
		    System.DebugLog "XoDrillSenders.timeSeries() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonInfo as new JSONItem( stringReturn )
		  
		  return jsonInfo
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function verifyDomain() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if domain.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "domain"
		    raise er
		  end if
		  if mailbox.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "mailbox"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"senders/verify-domain.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "domain" ) = domain
		  jsonParameters.Value( "mailbox" ) = mailbox
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillSenders.verifyDomain() " + stringReturn
		    System.DebugLog "XoDrillSenders.verifyDomain() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonInfo as new JSONItem( stringReturn )
		  
		  return jsonInfo
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		address As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h0
		domain As string
	#tag EndProperty

	#tag Property, Flags = &h0
		mailbox As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="address"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="domain"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mailbox"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
