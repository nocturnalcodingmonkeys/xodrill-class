#tag Class
Protected Class XoDrillTemplates
	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function list(filter as string = "") As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"templates/list.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  if filter.len > 0 then jsonParameters.Value( "label" ) = filter
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTemplates.list() " + stringReturn
		    System.DebugLog "XoDrillTemplates.list() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTemplates as new JSONItem( stringReturn )
		  
		  return jsonTemplates
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function listAsString(filter as string = "") As string()
		  
		  dim astrTempaltes() as string
		  dim jsonTemplates as new JSONItem
		  jsonTemplates = list( filter )
		  dim intCounter as integer = 0
		  while intCounter < jsonTemplates.Count
		    if jsonTemplates.Child( intCounter ).HasName( "name" ) then
		      System.DebugLog "XoDrillTemplates.list() " + jsonTemplates.Child( intCounter ).ToString
		      
		      astrTempaltes.Append jsonTemplates.Child( intCounter ).ToString
		    end if
		    
		    intCounter = intCounter+1
		  wend
		  
		  if astrTempaltes.Ubound > -1 then return astrTempaltes
		  
		  return NIL
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timeout"
			Group="Behavior"
			InitialValue="60"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
