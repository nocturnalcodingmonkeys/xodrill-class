#tag Class
Protected Class XoDrillMetadata
	#tag Method, Flags = &h0
		Function add() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if name.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "name"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"metadata/add.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" )  = APIkey
		  jsonParameters.Value( "name" ) = name
		  if viewTemplate.Len > 0 then jsonParameters.Value( "view_template" ) = viewTemplate
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMetadata.add() " + stringReturn
		    System.DebugLog "XoDrillMetadata.add() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function delete() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if name.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "name"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"metadata/delete.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" )  = APIkey
		  jsonParameters.Value( "name" ) = name
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMetadata.delete() " + stringReturn
		    System.DebugLog "XoDrillMetadata.delete() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function list() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"metadata/list.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMetadata.list() " + stringReturn
		    System.DebugLog "XoDrillMetadata.list() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function update() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if name.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "name"
		    raise er
		  end if
		  if viewTemplate.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "viewTemplate"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"metadata/update.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" )           = APIkey
		  jsonParameters.Value( "name" )          = name
		  jsonParameters.Value( "view_template" ) = viewTemplate
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMetadata.update() " + stringReturn
		    System.DebugLog "XoDrillMetadata.update() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h0
		name As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h0
		viewTemplate As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="viewTemplate"
			Group="Behavior"
			Type="string"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
