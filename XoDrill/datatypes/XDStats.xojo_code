#tag Class
Protected Class XDStats
	#tag Method, Flags = &h0
		Sub constructor()
		  clicks       = -1
		  complaints   = -1
		  hardBounces  = -1
		  softBounces  = -1
		  opens        = -1
		  rejects      = -1
		  sent         = -1
		  uniqueClicks = -1
		  uniqueOpens  = -1
		  unsubs       = -1
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		clicks As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		complaints As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		hardBounces As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		opens As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		rejects As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		sent As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		softBounces As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		uniqueClicks As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		uniqueOpens As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		unsubs As Integer
	#tag EndProperty


	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="clicks"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="complaints"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="hardBounces"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="opens"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rejects"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sent"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="softBounces"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uniqueClicks"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uniqueOpens"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unsubs"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
