#tag Class
Protected Class XDUserInfo
	#tag Method, Flags = &h0
		Sub constructor()
		  allTime     = new XDStats
		  last30days  = new XDStats
		  last60days  = new XDStats
		  last7days   = new XDStats
		  last90days  = new XDStats
		  today       = new XDStats
		  backlog     = -1
		  hourlyQuota = -1
		  publicId    = "unknown"
		  reputation  = -1
		  username    = "unknown"
		  createdAt   = new date
		  dim boolTest as boolean = ParseDate( "1969-01-01 01:01:01", createdAt )
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		allTime As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		backlog As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		createdAt As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		hourlyQuota As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		last30days As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		last60days As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		last7days As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		last90days As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		publicId As string
	#tag EndProperty

	#tag Property, Flags = &h0
		reputation As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		today As XDStats
	#tag EndProperty

	#tag Property, Flags = &h0
		username As string
	#tag EndProperty


	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="backlog"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="hourlyQuota"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="publicId"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="reputation"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
