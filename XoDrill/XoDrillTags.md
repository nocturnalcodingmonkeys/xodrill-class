# XoDrillTags class

base URL for the class: https://mandrillapp.com/api/1.0/tags/*

## completed methods

* allTimeSeries() as JSONItem
* delete() as JSONItem
* info() as JSONItem
* list() as JSONItem
* listAsXDTag()* as XDTag()
* timeSeries() as JSONItem

\* methods are methods that return a non-standard for Mandril API return type.


## valid as of

valid as of GIT: 0.5.0 (37)