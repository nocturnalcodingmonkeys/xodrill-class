# XoDrillSenders class

base URL for the class: https://mandrillapp.com/api/1.0/senders/*

## completed methods

* addDomain() as JSONItem
* checkDomain() as JSONItem
* domains() as JSONItem
* info() as JSONItem as JSONItem
* list() as JSONItem
* timeSeries() as JSONItem
* verifyDomain() as JSONItem


\* methods are methods that return a non-standard for Mandril API return type.


## valid as of

valid as of GIT: 0.5.0 (37)