# XoDrillUser class

base URL for the class: https://mandrillapp.com/api/1.0/users/*

## completed methods

* info() as JSONItem
* infoAsXDUserInfo()* as XDUserInfo
* ping() as String
* pingAsBoolean()* as Boolean
* ping2() as JSONItem
* ping2AsBoolean()* as Boolean
* senders() as JSONItem

\* methods are methods that return a non-standard for Mandril API return type.


## valid as of

valid as of GIT: 0.5.0 (37)