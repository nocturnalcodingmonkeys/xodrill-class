# XoDrillTemplate class

base URL for the class: https://mandrillapp.com/api/1.0/templates/*

## completed methods

* list() as JSONItem
* listAsString()* as string()


\* methods are methods that return a non-standard for Mandril API return type.

## TBD methods

* add()
* delete()
* info()
* update()
* time-series()
* publish()
* render()

## valid as of

valid as of GIT: 0.4.0 (34)