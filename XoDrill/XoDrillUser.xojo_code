#tag Class
Protected Class XoDrillUser
	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function info() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"users/info.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillUser.info() " + stringReturn
		    System.DebugLog "XoDrillUser.info() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function infoAsXDUserInfo() As XDUserInfo
		  return parseInfo( info )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function parseInfo(info as JSONItem) As XDUserInfo
		  dim xduiReturn as new XDUserInfo
		  
		  if info.HasName( "username" )     then xduiReturn.username    = info.Value( "username" )
		  if info.HasName( "public_id" )    then xduiReturn.publicId    = info.Value( "public_id" )
		  if info.HasName( "backlog" )      then xduiReturn.backlog     = info.Value( "backlog" )
		  if info.HasName( "reputation" )   then xduiReturn.reputation  = info.Value( "reputation" )
		  if info.HasName( "hourly_quota" ) then xduiReturn.hourlyQuota = info.Value( "hourly_quota" )
		  if info.HasName( "created_at" )   then
		    dim dateTemp as date
		    dim boolTest as boolean = ParseDate( info.Value( "created_at" ), dateTemp )
		    xduiReturn.createdAt = dateTemp
		  end if
		  if info.HasName( "stats") then
		    
		    if info.child( "stats" ).HasName( "today" ) then
		      if info.child( "stats" ).child( "today" ).HasName( "complaints" )    then xduiReturn.today.complaints   = info.child( "stats" ).child( "today" ).Value( "complaints" )
		      if info.child( "stats" ).child( "today" ).HasName( "sent" )          then xduiReturn.today.sent         = info.child( "stats" ).child( "today" ).Value( "sent" )
		      if info.child( "stats" ).child( "today" ).HasName( "hard_bounces" )  then xduiReturn.today.hardBounces  = info.child( "stats" ).child( "today" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "today" ).HasName( "soft_bounces" )  then xduiReturn.today.softBounces  = info.child( "stats" ).child( "today" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "today" ).HasName( "unsubs" )        then xduiReturn.today.unsubs       = info.child( "stats" ).child( "today" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "today" ).HasName( "opens" )         then xduiReturn.today.opens        = info.child( "stats" ).child( "today" ).Value( "opens" )
		      if info.child( "stats" ).child( "today" ).HasName( "unique_opens" )  then xduiReturn.today.uniqueOpens  = info.child( "stats" ).child( "today" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "today" ).HasName( "clicks" )        then xduiReturn.today.clicks       = info.child( "stats" ).child( "today" ).Value( "clicks" )
		      if info.child( "stats" ).child( "today" ).HasName( "rejects" )       then xduiReturn.today.rejects      = info.child( "stats" ).child( "today" ).Value( "rejects" )
		      if info.child( "stats" ).child( "today" ).HasName( "unique_clicks" ) then xduiReturn.today.uniqueClicks = info.child( "stats" ).child( "today" ).Value( "unique_clicks" )
		      
		    end if
		    
		    if info.child( "stats" ).HasName( "last_7_days" ) then
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "complaints" )    then xduiReturn.last7days.complaints   = info.child( "stats" ).child( "last_7_days" ).Value( "complaints" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "sent" )          then xduiReturn.last7days.sent         = info.child( "stats" ).child( "last_7_days" ).Value( "sent" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "hard_bounces" )  then xduiReturn.last7days.hardBounces  = info.child( "stats" ).child( "last_7_days" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "soft_bounces" )  then xduiReturn.last7days.softBounces  = info.child( "stats" ).child( "last_7_days" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "unsubs" )        then xduiReturn.last7days.unsubs       = info.child( "stats" ).child( "last_7_days" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "opens" )         then xduiReturn.last7days.opens        = info.child( "stats" ).child( "last_7_days" ).Value( "opens" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "unique_opens" )  then xduiReturn.last7days.uniqueOpens  = info.child( "stats" ).child( "last_7_days" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "clicks" )        then xduiReturn.last7days.clicks       = info.child( "stats" ).child( "last_7_days" ).Value( "clicks" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "rejects" )       then xduiReturn.last7days.rejects      = info.child( "stats" ).child( "last_7_days" ).Value( "rejects" )
		      if info.child( "stats" ).child( "last_7_days" ).HasName( "unique_clicks" ) then xduiReturn.last7days.uniqueClicks = info.child( "stats" ).child( "last_7_days" ).Value( "unique_clicks" )
		      
		    end if
		    
		    if info.child( "stats" ).HasName( "last_30_days" ) then
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "complaints" )    then xduiReturn.last30days.complaints   = info.child( "stats" ).child( "last_30_days" ).Value( "complaints" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "sent" )          then xduiReturn.last30days.sent         = info.child( "stats" ).child( "last_30_days" ).Value( "sent" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "hard_bounces" )  then xduiReturn.last30days.hardBounces  = info.child( "stats" ).child( "last_30_days" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "soft_bounces" )  then xduiReturn.last30days.softBounces  = info.child( "stats" ).child( "last_30_days" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "unsubs" )        then xduiReturn.last30days.unsubs       = info.child( "stats" ).child( "last_30_days" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "opens" )         then xduiReturn.last30days.opens        = info.child( "stats" ).child( "last_30_days" ).Value( "opens" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "unique_opens" )  then xduiReturn.last30days.uniqueOpens  = info.child( "stats" ).child( "last_30_days" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "clicks" )        then xduiReturn.last30days.clicks       = info.child( "stats" ).child( "last_30_days" ).Value( "clicks" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "rejects" )       then xduiReturn.last30days.rejects      = info.child( "stats" ).child( "last_30_days" ).Value( "rejects" )
		      if info.child( "stats" ).child( "last_30_days" ).HasName( "unique_clicks" ) then xduiReturn.last30days.uniqueClicks = info.child( "stats" ).child( "last_30_days" ).Value( "unique_clicks" )
		      
		    end if
		    
		    if info.child( "stats" ).HasName( "last_60_days" ) then
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "complaints" )    then xduiReturn.last60days.complaints   = info.child( "stats" ).child( "last_60_days" ).Value( "complaints" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "sent" )          then xduiReturn.last60days.sent         = info.child( "stats" ).child( "last_60_days" ).Value( "sent" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "hard_bounces" )  then xduiReturn.last60days.hardBounces  = info.child( "stats" ).child( "last_60_days" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "soft_bounces" )  then xduiReturn.last60days.softBounces  = info.child( "stats" ).child( "last_60_days" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "unsubs" )        then xduiReturn.last60days.unsubs       = info.child( "stats" ).child( "last_60_days" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "opens" )         then xduiReturn.last60days.opens        = info.child( "stats" ).child( "last_60_days" ).Value( "opens" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "unique_opens" )  then xduiReturn.last60days.uniqueOpens  = info.child( "stats" ).child( "last_60_days" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "clicks" )        then xduiReturn.last60days.clicks       = info.child( "stats" ).child( "last_60_days" ).Value( "clicks" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "rejects" )       then xduiReturn.last60days.rejects      = info.child( "stats" ).child( "last_60_days" ).Value( "rejects" )
		      if info.child( "stats" ).child( "last_60_days" ).HasName( "unique_clicks" ) then xduiReturn.last60days.uniqueClicks = info.child( "stats" ).child( "last_60_days" ).Value( "unique_clicks" )
		      
		    end if
		    
		    if info.child( "stats" ).HasName( "last_90_days" ) then
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "complaints" )    then xduiReturn.last90days.complaints   = info.child( "stats" ).child( "last_90_days" ).Value( "complaints" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "sent" )          then xduiReturn.last90days.sent         = info.child( "stats" ).child( "last_90_days" ).Value( "sent" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "hard_bounces" )  then xduiReturn.last90days.hardBounces  = info.child( "stats" ).child( "last_90_days" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "soft_bounces" )  then xduiReturn.last90days.softBounces  = info.child( "stats" ).child( "last_90_days" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "unsubs" )        then xduiReturn.last90days.unsubs       = info.child( "stats" ).child( "last_90_days" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "opens" )         then xduiReturn.last90days.opens        = info.child( "stats" ).child( "last_90_days" ).Value( "opens" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "unique_opens" )  then xduiReturn.last90days.uniqueOpens  = info.child( "stats" ).child( "last_90_days" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "clicks" )        then xduiReturn.last90days.clicks       = info.child( "stats" ).child( "last_90_days" ).Value( "clicks" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "rejects" )       then xduiReturn.last90days.rejects      = info.child( "stats" ).child( "last_90_days" ).Value( "rejects" )
		      if info.child( "stats" ).child( "last_90_days" ).HasName( "unique_clicks" ) then xduiReturn.last90days.uniqueClicks = info.child( "stats" ).child( "last_90_days" ).Value( "unique_clicks" )
		      
		    end if
		    
		    if info.child( "stats" ).HasName( "all_time" ) then
		      if info.child( "stats" ).child( "all_time" ).HasName( "complaints" )    then xduiReturn.allTime.complaints   = info.child( "stats" ).child( "all_time" ).Value( "complaints" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "sent" )          then xduiReturn.allTime.sent         = info.child( "stats" ).child( "all_time" ).Value( "sent" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "hard_bounces" )  then xduiReturn.allTime.hardBounces  = info.child( "stats" ).child( "all_time" ).Value( "hard_bounces" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "soft_bounces" )  then xduiReturn.allTime.softBounces  = info.child( "stats" ).child( "all_time" ).Value( "soft_bounces" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "unsubs" )        then xduiReturn.allTime.unsubs       = info.child( "stats" ).child( "all_time" ).Value( "unsubs" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "opens" )         then xduiReturn.allTime.opens        = info.child( "stats" ).child( "all_time" ).Value( "opens" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "unique_opens" )  then xduiReturn.allTime.uniqueOpens  = info.child( "stats" ).child( "all_time" ).Value( "unique_opens" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "clicks" )        then xduiReturn.allTime.clicks       = info.child( "stats" ).child( "all_time" ).Value( "clicks" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "rejects" )       then xduiReturn.allTime.rejects      = info.child( "stats" ).child( "all_time" ).Value( "rejects" )
		      if info.child( "stats" ).child( "all_time" ).HasName( "unique_clicks" ) then xduiReturn.allTime.uniqueClicks = info.child( "stats" ).child( "all_time" ).Value( "unique_clicks" )
		      
		    end if
		    
		  end if
		  
		  return xduiReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ping() As string
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"users/ping.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillUser.ping() " + stringReturn
		    System.DebugLog "XoDrillUser.ping() " + cstr( stringReturn.Len )
		    System.DebugLog "XoDrillUser.ping() " + cstr( strcomp( stringReturn,chr(34)+"PONG!"+chr(34),0 ) )
		    System.DebugLog "XoDrillUser.ping() " + cstr( strcomp( stringReturn,chr(34)+"PONG!"+chr(34),0 ) = 0 )
		  #Endif
		  
		  if stringReturn.InStr( chr(34)+"status"+chr(34) ) > 0 then
		    dim jsonTemp as new JSONItem( stringReturn)
		    if jsonTemp.HasName( "status" ) AND strcomp( jsonTemp.Value( "status" ), "error",0 ) = 0 then
		      dim er as new XDGeneralException
		      if jsonTemp.HasName( "code" )    then er.ErrorNumber = jsonTemp.Value( "code" ).IntegerValue
		      if jsonTemp.HasName( "name" )    then er.name        = jsonTemp.Value( "name" ).StringValue
		      if jsonTemp.HasName( "message" ) then er.Message     = jsonTemp.Value( "message" ).StringValue
		      raise er
		    end if
		    
		  end if
		  
		  return stringReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ping2() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"users/ping2.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillUser.ping2() " + stringReturn
		    System.DebugLog "XoDrillUser.ping2() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTemp as new JSONItem( stringReturn)
		  if jsonTemp.HasName( "status" ) AND strcomp( jsonTemp.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonTemp.HasName( "code" )    then er.ErrorNumber = jsonTemp.Value( "code" ).IntegerValue
		    if jsonTemp.HasName( "name" )    then er.name        = jsonTemp.Value( "name" ).StringValue
		    if jsonTemp.HasName( "message" ) then er.Message     = jsonTemp.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonTemp
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ping2AsBoolean() As boolean
		  dim jsonTemp as JSONItem = ping2
		  
		  if jsonTemp.HasName( "PING" ) AND StrComp( jsonTemp.Value( "PING" ),"PONG!",0 ) = 0 then
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function pingAsBoolean() As Boolean
		  dim stringReturn as String = ping
		  
		  if stringReturn.Len > 0 AND strcomp( stringReturn,chr(34)+"PONG!"+chr(34),0 ) = 0 then
		    return true
		  else
		    return false
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function senders() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"users/senders.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillUser.senders() " + stringReturn
		    System.DebugLog "XoDrillUser.senders() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timeout"
			Group="Behavior"
			InitialValue="60"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
