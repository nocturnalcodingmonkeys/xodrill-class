# XoDrillIps class

base URL for the class: https://mandrillapp.com/api/1.0/ips/*

## completed methods

* cancelWarmup() as JSONItem
* checkCustomDNS() as JSONItem
* delete() as JSONItem
* info() as JSONItem
* list() as JSONItem
* listPools() as JSONItem
* setCustomDNS() as JSONItem
* startWarmup() as JSONItem


\* methods are methods that return a non-standard for Mandril API return type.

\** method to build a message from the properties of the class so it can be sent with send().

## TBD methods

* provision()
* setPool()
* createPool()
* deletePool()


## valid as of

valid as of GIT: 0.5.0 (37)