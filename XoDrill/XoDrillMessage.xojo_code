#tag Class
Protected Class XoDrillMessage
	#tag Method, Flags = &h0
		Sub buildMessage()
		  
		  dim jsonMessage as new JSONItem
		  if subject.len     > 0 then jsonMessage.Value( "subject" )      = subject
		  if messageHTML.len > 0 then jsonMessage.Value( "html" )         = messageHTML
		  if messageTEXT.len > 0 then jsonMessage.Value( "text" )         = messageTEXT
		  if fromEMAIL.len   > 0 then jsonMessage.Value( "from_email" )   = fromEMAIL
		  if fromNAME.len    > 0 then jsonMessage.Value( "from_name" )    = fromNAME
		  if bccAddress.len  > 0 then jsonMessage.Value( "bcc_address" )  = bccAddress
		  
		  if tags.Ubound < 0 then
		    // issue at hand, as this is required.
		  else
		    dim jsonTags as new JSONItem
		    for each oTag as string in tags
		      jsonTags.append(  oTag )
		    next
		    jsonMessage.value( "tags" ) = jsonTags
		  end if
		  
		  // faking the TO's
		  dim jsonTO as new JSONItem
		  for each oTo as toClass in tos
		    dim jsonTOsingle as new JSONItem
		    jsonTOsingle.Value( "email" ) = oTo.email
		    jsonTOsingle.Value( "name" )  = oTo.name
		    jsonTOsingle.Value( "type" )  = oTo.type
		    
		    jsonTO.append( jsonTOsingle )
		  next
		  //
		  jsonMessage.Value( "to" ) = jsonTO
		  
		  dim jsonReturn as new JSONItem
		  if APIkey.Len > 0 then jsonReturn.Value( "key" )     = APIkey
		  jsonReturn.Value( "message" ) = jsonMessage
		  
		  SYstem.DebugLog "XoDrillMessage.buildMessage() " + jsonReturn.ToString
		  SYstem.DebugLog "XoDrillMessage.buildMessage() " + jsonReturn.ToString.ReplaceAll( chr(92),"" )
		  builtMessage = jsonReturn
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function cancelScheduled() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if builtMessage = NIL then
		    dim er as XDGeneralException
		    er.Message = "you need to first build the message"
		    raise er
		  end if
		  if id.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "id"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"messages/cancel-scheduled.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "id" ) = id
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMessage.cancelScheduled() " + stringReturn
		    System.DebugLog "XoDrillMessage.cancelScheduled() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name        = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message     = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function content() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if id.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "id"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"messages/content.json"
		  
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "id" ) = id
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMessage.content() " + stringReturn
		    System.DebugLog "XoDrillMessage.content() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name        = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message     = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function info() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if builtMessage = NIL then
		    dim er as XDGeneralException
		    er.Message = "you need to first build the message"
		    raise er
		  end if
		  if id.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "id"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"messages/info.json"
		  
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "id" ) = id
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMessage.info() " + stringReturn
		    System.DebugLog "XoDrillMessage.info() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name        = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message     = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function listScheduled() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"messages/list-scheduled.json"
		  
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMessage.listScheduled() " + stringReturn
		    System.DebugLog "XoDrillMessage.listScheduled() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name        = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message     = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function send() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if builtMessage = NIL then
		    dim er as XDGeneralException
		    er.Message = "you need to first build the message"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"messages/send.json"
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( builtMessage.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillMessage.send() " + stringReturn
		    System.DebugLog "XoDrillMessage.send() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonReturn as new JSONItem( stringReturn )
		  if jsonReturn.HasName( "status" ) AND strcomp( jsonReturn.Value( "status" ), "error",0 ) = 0 then
		    dim er as new XDGeneralException
		    if jsonReturn.HasName( "code" )    then er.ErrorNumber = jsonReturn.Value( "code" ).IntegerValue
		    if jsonReturn.HasName( "name" )    then er.name        = jsonReturn.Value( "name" ).StringValue
		    if jsonReturn.HasName( "message" ) then er.Message     = jsonReturn.Value( "message" ).StringValue
		    raise er
		  end if
		  
		  return jsonReturn
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h0
		bccAddress As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private builtMessage As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		fromEMAIL As string
	#tag EndProperty

	#tag Property, Flags = &h0
		fromNAME As string
	#tag EndProperty

	#tag Property, Flags = &h0
		id As string
	#tag EndProperty

	#tag Property, Flags = &h0
		messageHTML As string
	#tag EndProperty

	#tag Property, Flags = &h0
		messageTEXT As string
	#tag EndProperty

	#tag Property, Flags = &h0
		subject As string
	#tag EndProperty

	#tag Property, Flags = &h0
		tags() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h0
		tos() As toClass
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="bccAddress"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fromEMAIL"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fromNAME"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="id"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="messageHTML"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="messageTEXT"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="subject"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timeout"
			Group="Behavior"
			InitialValue="60"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
