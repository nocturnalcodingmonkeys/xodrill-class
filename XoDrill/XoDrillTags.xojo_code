#tag Class
Protected Class XoDrillTags
	#tag Method, Flags = &h0
		Function allTimeSeries() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"tags/all-time-series.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTags.allTimeSeries() " + stringReturn
		    System.DebugLog "XoDrillTags.allTimeSeries() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function delete() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if tag.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "tag"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"tags/delete.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "tag" ) = tag
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTags.delete() " + stringReturn
		    System.DebugLog "XoDrillTags.delete() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function info() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if tag.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "tag"
		  end if
		  dim stringURL as string = kMandrillAPIbaseURL+"tags/info.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "tag" ) = tag
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTags.info() " + stringReturn
		    System.DebugLog "XoDrillTags.info() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function list() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"tags/list.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTags.list() " + stringReturn
		    System.DebugLog "XoDrillTags.list() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function listAsString() As string()
		  dim strReturnValue() as string
		  
		  for each aTag as XDTag in listAsXDTag
		    strReturnValue.Append aTag.name
		    
		  next
		  
		  if strReturnValue.Ubound > -1 then return strReturnValue
		  
		  return array( "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function listAsXDTag() As XDTag()
		  
		  
		  dim aTags() as XDTag
		  dim jsonTags as new JSONItem
		  jsonTags = list
		  dim intCounter as integer = 0
		  while intCounter < jsonTags.Count
		    if jsonTags.Child( intCounter ).HasName( "tag" ) then
		      dim oTag as new XDTag
		      System.DebugLog "XoDrillTags.list() " + jsonTags.Child( intCounter ).ToString
		      oTag.name = jsonTags.Child( intCounter ).Value( "tag" )
		      if jsonTags.Child( intCounter ).HasName( "opens" )         then oTag.stats.opens        = jsonTags.Child( intCounter ).Value( "opens" )
		      if jsonTags.Child( intCounter ).HasName( "rejects" )       then oTag.stats.rejects      = jsonTags.Child( intCounter ).Value( "rejects" )
		      if jsonTags.Child( intCounter ).HasName( "complaints" )    then oTag.stats.complaints   = jsonTags.Child( intCounter ).Value( "complaints" )
		      if jsonTags.Child( intCounter ).HasName( "hard_bounces" )  then oTag.stats.hardBounces  = jsonTags.Child( intCounter ).Value( "hard_bounces" )
		      if jsonTags.Child( intCounter ).HasName( "unique_clicks" ) then oTag.stats.uniqueClicks = jsonTags.Child( intCounter ).Value( "unique_clicks" )
		      if jsonTags.Child( intCounter ).HasName( "soft_bounces" )  then oTag.stats.softBounces  = jsonTags.Child( intCounter ).Value( "soft_bounces" )
		      if jsonTags.Child( intCounter ).HasName( "unique_opens" )  then oTag.stats.uniqueOpens  = jsonTags.Child( intCounter ).Value( "unique_opens" )
		      'if jsonTags.Child( intCounter ).HasName( "reputation" ) then oTag.stats.?? = jsonTags.Child( intCounter ).Value( "reputation" )
		      if jsonTags.Child( intCounter ).HasName( "clicks" )        then oTag.stats.clicks       = jsonTags.Child( intCounter ).Value( "clicks" )
		      if jsonTags.Child( intCounter ).HasName( "sent" )          then oTag.stats.sent         = jsonTags.Child( intCounter ).Value( "sent" )
		      if jsonTags.Child( intCounter ).HasName( "unsubs" )        then oTag.stats.unsubs       = jsonTags.Child( intCounter ).Value( "unsubs" )
		      
		      aTags.Append oTag
		    end if
		    
		    intCounter = intCounter+1
		  wend
		  
		  if aTags.Ubound > -1 then return aTags
		  
		  return NIL
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function timeSeries() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if tag.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "tag"
		    raise er
		  end if
		  dim stringURL as string = kMandrillAPIbaseURL+"tags/time-series.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "tag" ) = tag
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillTags.timeSeries() " + stringReturn
		    System.DebugLog "XoDrillTags.timeSeries() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h0
		tag As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="tag"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
