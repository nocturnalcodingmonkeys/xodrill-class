#tag Class
Protected Class XoDrillIps
	#tag Method, Flags = &h0
		Function cancelWarmup() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/cancel-warmup.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.cancelWarmup() " + stringReturn
		    System.DebugLog "XoDrillIps.cancelWarmup() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function checkCustomDNS() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if domain.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "domain"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/check-custom-dns.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  jsonParameters.Value( "domain" ) = domain
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.checkCustomDNS() " + stringReturn
		    System.DebugLog "XoDrillIps.checkCustomDNS() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub constructor(mandrillapikey as string)
		  if mandrillapikey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  XDHTTP = new HTTPSecureSocket
		  APIkey = mandrillapikey
		  
		  XDHTTP.Secure=true
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.ConnectionType = HTTPSecureSocket.TLSv1
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function delete() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/delete.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.delete() " + stringReturn
		    System.DebugLog "XoDrillIps.delete() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function info() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/info.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.info() " + stringReturn
		    System.DebugLog "XoDrillIps.info() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function list() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/list.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.list() " + stringReturn
		    System.DebugLog "XoDrillIps.list() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function listPools() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/list-pools.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.listPools() " + stringReturn
		    System.DebugLog "XoDrillIps.listPools() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function setCustomDNS() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if domain.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "domain"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/set-custom-dns.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  jsonParameters.Value( "domain" ) = domain
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.setCustomDNS() " + stringReturn
		    System.DebugLog "XoDrillIps.setCustomDNS() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function startWarmup() As JSONItem
		  if APIkey.Len < 1 then
		    dim er as new XDNoApiKeyException
		    er.Message = "no API key defined"
		    raise er
		  end if
		  if ip.Len < 1 then
		    dim er as new XDMissingManditoryPropertyException
		    er.key = "ip"
		    raise er
		  end if
		  
		  dim stringURL as string = kMandrillAPIbaseURL+"ips/start-warmup.json"
		  dim jsonParameters as new JSONItem
		  jsonParameters.Value( "key" ) = APIkey
		  jsonParameters.Value( "ip" )  = ip
		  
		  XDHTTP.ClearRequestHeaders
		  XDHTTP.SetRequestContent( jsonParameters.ToString, "application/json; charset=utf-8" )
		  dim stringReturn as string = XDHTTP.Post( stringURL,timeout )
		  
		  if XDHTTP.ErrorCode = 102 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=102
		    ex.Message = "socket connection issue."
		    raise ex
		  end if
		  if XDHTTP.ErrorCode = -1 then
		    dim ex as new XDConnectionIssueException
		    ex.ErrorNumber=-1
		    ex.Message = "socket timed out."
		    raise ex
		  end if
		  
		  #if DebugBuild
		    System.DebugLog "XoDrillIps.startWarmup() " + stringReturn
		    System.DebugLog "XoDrillIps.startWarmup() " + cstr( stringReturn.Len )
		  #Endif
		  
		  dim jsonTags as new JSONItem( stringReturn )
		  
		  return jsonTags
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIkey As string
	#tag EndProperty

	#tag Property, Flags = &h0
		domain As string
	#tag EndProperty

	#tag Property, Flags = &h0
		ip As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeout As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private XDHTTP As HTTPSecureSocket
	#tag EndProperty


	#tag Constant, Name = kMandrillAPIbaseURL, Type = String, Dynamic = False, Default = \"https://mandrillapp.com/api/1.0/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVersion, Type = String, Dynamic = False, Default = \"0.5.0 (37)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="domain"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ip"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
